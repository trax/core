hunter_config(
    cryptopp
    VERSION
    "8.2.0-neuro"
    URL
    "https://github.com/hunter-packages/cryptopp/archive/v8.2.0-p0.tar.gz"
    SHA1
    38a70c9ba970cc862b5cca0010fffdd4e56afcae
)
